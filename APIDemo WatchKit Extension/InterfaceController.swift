//
//  InterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Parrot on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    @IBOutlet var TableView: WKInterfaceTable!
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    var notify = ""
    var todayORtomorrow = ""
    var URLList:[URLObject] = []
    var NotifyArray = [String]()
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        // Play a "click" sound when you get the message
        WKInterfaceDevice().play(.click)
        
        // output a debug message to the terminal
        print("Got a message!")
//        print(message)
        
        // get the URL from the phone
        for (name, URL) in message { // I celebrate message and check it
            print("game: '\(name)' : '\(URL)'.")
            print("\(URL)")
            
            let URL = "\(URL)"
            print("Url: \(URL)")
            
            Alamofire.request(URL).responseJSON {
                // 1. store the data from the internet in the
                // response variable
                response in
                
                // 2. get the data out of the variable
                guard let apiData = response.result.value else {
                    print("Error getting data from the URL")
                    return
                }
                
                let jsonResponse = JSON(apiData)
                let Data = jsonResponse["data"].arrayValue
                
                
                
                for eachData in Data {
                    
                    let cityFrom = eachData["cityFrom"].string
                    let cityTo = eachData["cityTo"].string
                    let price = eachData["price"].int
                    let flyDuration = eachData["fly_duration"].string
                    let flyTo = eachData["flyTo"].string
                    let flyFrom = eachData["flyFrom"].string
                    let timeFly = eachData["dTime"].int
                    print(cityFrom!)
                    print(cityTo!)
                    print(price!)
                    print(flyDuration!)
                    print("airport Stop: \(flyTo!)")
                    print("airport Start: \(flyFrom!)")
                    print("Fly Time: \(timeFly!)")
                    
                    //TimeStamp
                    let timeInterval = timeFly!
                    print("time interval is \(timeFly!)")
                    
                    //Convert to Date
                    let date = NSDate(timeIntervalSince1970: TimeInterval(timeInterval))
                    
                    //Date formatting
                    let dateFormatter = DateFormatter()
                    
                    dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
                    dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
                    let dateString = dateFormatter.string(from: date as Date)
                    print("Fly Time: \(dateString)")
                    
                    //=============================== COMPONENTS API =====================//
                    // get component of day
                    let dateFormatter1 = DateFormatter()
                    dateFormatter1.dateFormat = "dd"
                    dateFormatter1.timeZone = NSTimeZone(name: "UTC")! as TimeZone
                    let Day = dateFormatter1.string(from: date as Date)
                    
                    print(Day)
                    
                    // get component of Month
                    let dateFormatter2 = DateFormatter()
                    dateFormatter2.dateFormat = "MM"
                    dateFormatter2.timeZone = NSTimeZone(name: "UTC")! as TimeZone
                    let Month = dateFormatter2.string(from: date as Date)
                  
                    print(Month)
                    
                    // get component of Year
                    let dateFormatter3 = DateFormatter()
                    dateFormatter3.dateFormat = "yyyy"
                    dateFormatter3.timeZone = NSTimeZone(name: "UTC")! as TimeZone
                    let Year = dateFormatter3.string(from: date as Date)
                    
                    print(Year)
                    
                    // get component of Hour
                    let dateFormatter4 = DateFormatter()
                    dateFormatter4.dateFormat = "HH"
                    dateFormatter4.timeZone = NSTimeZone(name: "UTC")! as TimeZone
                    let Hour = dateFormatter4.string(from: date as Date)
                    
                    print(Hour)
                    
                    // get component of Minute
                    let dateFormatter5 = DateFormatter()
                    dateFormatter5.dateFormat = "mm"
                    dateFormatter5.timeZone = NSTimeZone(name: "UTC")! as TimeZone
                    let Minute = dateFormatter5.string(from: date as Date)
                    
                    print(Minute)
                    
                    
                    /////////////////////////////////  END COMPONENTS OF API ///////////////////////////////
                    
                    //=============================== COMPONENTS CURRENT TIME =====================//
                    // get the current date and time
                    let currentDateTime = Date()
                    
                    // get the user's calendar
                    let userCalendar = Calendar.current
                    
                    // choose which date and time components are needed
                    let requestedComponents: Set<Calendar.Component> = [
                        .year,
                        .month,
                        .day,
                        .hour,
                        .minute,
                        
                    ]
                    
                    let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: currentDateTime)
                    
                    let CurrentYear = dateTimeComponents.year!
                    let CurrentMonth = dateTimeComponents.month!
                    let CurrentDay = dateTimeComponents.day!
                    let CurrentHour = dateTimeComponents.hour!
                    let CurrentMinute = dateTimeComponents.minute!
                    //////////////////////  END COMPONENTS OF CURRENT TIME ///////////////////////////////
                    
                    print(CurrentHour)
                    // Notifications - Time to fly

                    //UNCOMMENT TO CHECK THEM
                    //check code if it's work or not
                    
                    //test if time : 8:00
                    if (Int(Day) == 19 && Int(Month) == 03 && Int(Year) == 2019 && Int(Hour) == (06 + 02) && Int(Minute) == 0 ){
                        print("CHECK IN")
                        
                        self.notify = "CHECK IN"
                        
                        
                    }
                        //test If time : 7:30
                    else if (Int(Day) == 19 && Int(Month) == 03 && Int(Year) == 2019 && Int(Hour) == (06 + 01) && Int(Minute) == 30 ){
                        print("CHECK IN")
                        
                        self.notify = "CHECK IN"
                       
                        
                    }
//                     else if (Int(Day) == 19 && Int(Month) == 03 && Int(Year) == 2019 && Int(Hour) == (07 + 01) && Int(Minute) == 0 ){
//                        print("GO TO THE GATE")
//                        self.notify = "GOTOTHE GATE"
//
//                    }
//                    else if (Int(Day) == 19 && Int(Month) == 03 && Int(Year) == 2019 && Int(Hour) == 08 && Int(Minute) == 0 && Int(Minute) == 0){
//                        print("NOW BOARDING")
//                       self.notify = "NOW BOARDING"
//                    }
//                    else if (Int(Day) == 19 && Int(Month) == 03 && Int(Year) == 2019 && Int(Hour) == 08 && Int(Minute) == 0 && Int(Minute) == 0){
//                        print("IN FLIGHT")
//                        self.notify = "IN FLIGHT"
//                    }
                    // ----------------------END CHECK-------------------------\\\


//                    // real code (CHECK IN)
//                    if (Int(Day) == CurrentDay && Int(Month) == CurrentMonth && Int(Year) == CurrentYear && Int(Hour) == (CurrentHour + 02)){
//                        print("CHECK IN TIME")
//
//                    }
                    // GO TO THE GATE
                    else if (Int(Day) == CurrentDay && Int(Month) == CurrentMonth && Int(Year) == CurrentYear && Int(Hour) == (CurrentHour + 01)){
                        print("GO TO THE GATE")
                        
                        self.notify = "GO TO THE GATE"
                        
                    }
                    // NoW BOARDING
                    else if (Int(Day) == CurrentDay && Int(Month) == CurrentMonth && Int(Year) == CurrentYear && Int(Hour) == (CurrentHour - 01) && Int(Minute) == CurrentMinute + 30){
                        print("NOW BOARDING")
                        self.notify = "NOW BOARDING"
                        
                    }
                    // IN FLIGHT
                    else if (Int(Day) == CurrentDay && Int(Month) == CurrentMonth && Int(Year) == CurrentYear && Int(Hour) == (CurrentHour) && Int(Minute) == CurrentMinute){
                        print("IN FLIGHT")
                       self.notify = "IN FLIGHT"
                    }
                    else {
                        print("I got nothing :( ")
                        self.notify = ""

                    }
                    
                    
                    
                    // CHECK AND UI DISPLAY - FLIGHT TODAY
                     if (Int(Day) == CurrentDay && Int(Month) == CurrentMonth && Int(Year) == CurrentYear){
                        print("FLIGHT TODAY")
                        self.todayORtomorrow = "UPCOMING TRiP TODAY"
                    }
                        //CHECK AND UI DISPLAY - FLIGHT TOMORROW
                    else if (Int(Day) == (CurrentDay + 1) && Int(Month) == CurrentMonth && Int(Year) == CurrentYear){
                        print("FLIGHT TOMORROW")
                        self.todayORtomorrow = "UPCOMING TRiP TOMORROW"
                    }
                     else {
                        print("No Flight today or tomorrow")
                    }

                    print(self.NotifyArray)
                    
                    
                    let List = URLObject(cityFrom: cityFrom!, cityTo: cityTo!, Price: price!, flyDuration: flyDuration!, airportStart: flyTo!, airportStop: flyFrom!,flyat: dateString, notify: self.notify, todayORtomorrow: self.todayORtomorrow)
                    
                    
                        self.URLList.append(List)
            
                    
                    self.TableView.setNumberOfRows(self.URLList.count, withRowType:"myRow")
                    // 2. populate the table
                    // 2a. tell ios how many rows are in the table
                    // 2b. put the data into the row
                    // ---------
                    
                    // 1. loop through your array
                    // 2. take each item in the array and put it in a table row
                    for (i, g) in self.URLList.enumerated() {
                        let row = self.TableView.rowController(at: i) as! URLObject
                        
                       
                        row.label1.setText("From City: \(g.cityFrom!)")
                        row.label2.setText("To City: \(g.cityTo!)")
                        row.label3.setText("Price: \(g.Price!)")
                        row.label4.setText("Start Airport: \(g.airportStart!)")
                        row.label5.setText("Stop Airport: \(g.airportStop!)")
                        row.label6.setText("Fly Duration: \(g.flyDuration!)")
                        row.label7.setText("Fly at: \(g.flyat!)")
                        
                        row.NotificationLabel.setText("\(g.notify!)")
                        row.todayORtomorrowLabel.setText("\(g.todayORtomorrow!)")
                        
                       
                        
                    }
                    
                    
                }
                
            }
            
        }
    }
    

    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
            
        }
        
        // TODO: Put your API call here
    }
   
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    

}
