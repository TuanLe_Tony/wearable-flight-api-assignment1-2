//
//  URLObject.swift
//  APIDemo WatchKit Extension
//
//  Created by levantuan on 2019-03-16.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit

class URLObject: NSObject {
    @IBOutlet var label1: WKInterfaceLabel!
    @IBOutlet var label2: WKInterfaceLabel!
    @IBOutlet var label3: WKInterfaceLabel!
    @IBOutlet var label4: WKInterfaceLabel!
    @IBOutlet var label5: WKInterfaceLabel!
    @IBOutlet var label6: WKInterfaceLabel!
    @IBOutlet var label7: WKInterfaceLabel!
    
    @IBOutlet var todayORtomorrowLabel: WKInterfaceLabel!
    
    @IBOutlet var NotificationLabel: WKInterfaceLabel!
    var cityFrom:String?
    var cityTo:String?
    var Price:Int?
    var flyDuration:String?
    var airportStart:String?
    var airportStop:String?
    var flyat:String?
    var notify:String?
    var todayORtomorrow:String?
    
    // MARK: contructor
    convenience override init() {
        self.init(cityFrom:"", cityTo:"", Price:0, flyDuration:"",airportStart:"",airportStop:"",flyat:"",notify:"",todayORtomorrow:"" )
    }
    
    init(cityFrom:String, cityTo:String, Price:Int, flyDuration:String, airportStart:String, airportStop:String, flyat:String, notify:String,todayORtomorrow:String) {
        
        self.cityFrom = cityFrom
        self.cityTo = cityTo
        self.Price = Price
        self.flyDuration = flyDuration
        self.airportStart = airportStart
        self.airportStop = airportStop
        self.flyat = flyat
        
        self.notify = notify
        self.todayORtomorrow = todayORtomorrow
        
    }
    
}
