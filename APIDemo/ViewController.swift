//
//  ViewController.swift
//  APIDemo
//
//  Created by Parrot on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    

    @IBOutlet weak var TextView: UITextView!
    @IBOutlet weak var FirstPointTextField: UITextField!
    @IBOutlet weak var EndPointTextField: UITextField!
    @IBOutlet weak var PriceFromTextField: UITextField!
    @IBOutlet weak var PriceToTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (WCSession.isSupported()) {
            print("Yes it is!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
            
        }

        // Do any additional setup after loading the view, typically from a nib.
        
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func SearchButtonClicked(_ sender: Any) {
        if (WCSession.default.isReachable) {
            
        let URL = "https://api.skypicker.com/flights?flyFrom=\(FirstPointTextField.text!)&to=\(EndPointTextField.text!)&dateFrom=18/03/2019&dateTo=19/03/2019&price_from=\(PriceFromTextField.text!)&price_to=\(PriceToTextField.text!)&"
        print("Url: \(URL)")
        Alamofire.request(URL).responseJSON {
            // 1. store the data from the internet in the
            // response variable
            response in
            
            // 2. get the data out of the variable
            guard let apiData = response.result.value else {
                print("Error getting data from the URL")
                return
            }
            
            let jsonResponse = JSON(apiData)
            let Data = jsonResponse["data"].arrayValue
            
            
            //send message to watch / instead of cities i sent the whole URL
            let sendURL = ["URL": URL]
            // send the message to the watch
            WCSession.default.sendMessage(sendURL, replyHandler: nil)
            
            for eachData in Data {
                
                let cityFrom = eachData["cityFrom"].string
                let cityTo = eachData["cityTo"].string
                let price = eachData["price"].int
                let flyDuration = eachData["fly_duration"].string
                let flyTo = eachData["flyTo"].string
                let flyFrom = eachData["flyFrom"].string
                print(cityFrom!)
                print(cityTo!)
                print(price!)
                print(flyDuration!)
                print("airport Stop: \(flyTo!)")
                print("airport Start: \(flyFrom!)")
                
                print(jsonResponse)
                
                self.TextView.text = "PLEASE! WAITING RESULT FROM YOUR WATCH APP"
               

                }
            
            }
        }
    }
    
}

